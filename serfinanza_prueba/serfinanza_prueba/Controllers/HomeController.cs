﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using serfinanza_prueba.Data;
using serfinanza_prueba.Models;


namespace serfinanza_prueba.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()

        {
            List<country> countries = new List<country>();

            try
            {
                var httpClient = new HttpClient();
                var json = await httpClient.GetStringAsync("http://country.io/names.json");
                JObject o = JObject.Parse(json);

                serfinanzaEntities context = new serfinanzaEntities();




                foreach (JProperty item in o.Properties())
                {

                    country country = new country();

                    country.name = item.Value.ToString();


                    countries.Add(country);
                }
            }
            catch (Exception)
            {

                throw;
            }








            return View(countries);
        }

        [HttpPost]
        public ActionResult saveContact(string fullName, string country, Int64 phone, string email)
        {

            serfinanzaEntities context = new serfinanzaEntities();
            cliente cliente = new cliente
            {
                FullName = fullName,
                country = country,
                phone = phone,
                email = email
            };

            context.cliente.Add(cliente);
            context.SaveChanges();

            return RedirectToAction(nameof(HomeController.Contact));

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            serfinanzaEntities context = new serfinanzaEntities();
            List<Cliente> clients = new List<Cliente>();
          
            List<cliente>clientsDB = context.cliente.ToList();
            
            try
            {
               foreach(var item in clientsDB)
                {

                    Cliente client = new Cliente();

                    client.id = item.Id;
                    client.fullName = item.FullName;
                    client.phone = Int64.Parse(item.phone.ToString());
                    client.email = item.email;
                    client.country = item.country;


                    clients.Add(client);
                }


            }
            catch (Exception)
            {

                throw;
            }

            return View(clients);
        }
    }
}