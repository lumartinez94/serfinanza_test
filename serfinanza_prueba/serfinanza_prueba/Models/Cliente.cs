﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace serfinanza_prueba.Models
{
    public class Cliente
    {

        public int id { get; set; }
        public string fullName { get; set; }
        public Int64 phone { get; set; }
        public string email { get; set; }

        public string country { get; set; }
    }
}